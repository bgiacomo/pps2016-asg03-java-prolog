package u07asg

/**
  * Created by Giacomo on 29/04/2017.
  */
class Util() {

  var x: Int = 0
  var y: Int = 0

  def convertIndexFromPrologToJava(prologIndex: Int) {
    prologIndex match {
      case 0 =>
        y = 0
        x = 0
      case 1 =>
        y = 0
        x = 1
      case 2 =>
        y = 0
        x = 2
      case 3 =>
        y = 1
        x = 0
      case 4 =>
        y = 1
        x = 1
      case 5 =>
        y = 1
        x = 2
      case 6 =>
        y = 2
        x = 0
      case 7 =>
        y = 2
        x = 1
      case 8 =>
        y = 2
        x = 2
    }
  }

}