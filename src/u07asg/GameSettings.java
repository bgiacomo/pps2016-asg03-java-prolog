package u07asg;

/**
 * Created by Giacomo on 25/04/2017.
 */
public class GameSettings {

    private PlayerSettings playerSettings;
    private ComputerSettings computerSettings;

    private static GameSettings ourInstance = new GameSettings();

    public static GameSettings getInstance() {
        return ourInstance;
    }

    private GameSettings() {
        System.out.println("Gioco in modalità classica");
        playerSettings=new PlayerSettings();
        computerSettings=null;
    }

    public void switchToClassicMode(){
       System.out.println("Gioco in modalità classica");
       playerSettings.setClassicMode(true);
       computerSettings=null;

    }

    public void switchToVerticalMode(){
        System.out.println("Tris solo in verticale!");
        playerSettings.setVerticalMode(true);
        computerSettings=null;

    }

    public void switchToHorizontalMode(){
        System.out.println("Tris solo in orizzontale!");
        playerSettings.setHorizontalMode(true);
        computerSettings=null;
    }

    public void switchToComputerDummyMode(){
        System.out.println("1 vs cpu: dummy mode!");
        computerSettings=new ComputerSettings();
        computerSettings.setDummy(true);
    }

    public void switchToComputerAvgMode(){
        System.out.println("1 vs cpu: avg mode!");
        computerSettings=new ComputerSettings();
        computerSettings.setAverage(true);
    }

    public void switchToComputerHardMode(){
        System.out.println("1 vs cpu: hard mode!");
        computerSettings=new ComputerSettings();
        computerSettings.setHard(true);
    }


    public String checkMode(){

        if(playerSettings!=null && computerSettings==null){
            if (playerSettings.isClassicMode()) return "classic";
            if(playerSettings.isVerticalMode()) return "vertical";
            if (playerSettings.isHorizontalMode()) return "horizontal";
        }else {
            if(computerSettings.isDummy()) return  "computer";
            if(computerSettings.isAverage()) return  "avg";
            if(computerSettings.isHard()) return  "hard";
        }
        return "";
    }
}
