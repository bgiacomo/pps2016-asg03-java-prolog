package u07asg;

/**
 * Created by Giacomo on 26/04/2017.
 */
public class ComputerSettings {

    private boolean dummy;
    private boolean average;
    private boolean hard;

    public ComputerSettings() {
        this.dummy = true;
        this.average=false;
        this.hard=false;
    }

    public boolean isDummy() {
        return dummy;
    }

    public boolean isAverage() {
        return average;
    }

    public boolean isHard() {
        return hard;
    }

    public void setDummy(boolean dummy) {
        this.dummy = dummy;
        this.average=false;
        this.hard=false;
    }

    public void setAverage(boolean average) {
        this.average = average;
        this.dummy=false;
        this.hard=false;
    }

    public void setHard(boolean hard) {
        this.hard = hard;
        this.average=false;
        this.dummy=false;
    }
}
