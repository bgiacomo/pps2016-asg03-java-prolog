package u07asg;


import java.util.Optional;

/** A not so nicely engineered App to run TTT games, not very important.
 * It just works..
 */
public class TicTacToeApp {

    private TTTView myView;
    private final TicTacToe ttt;
    private boolean finished = false;
    private Player turn = Player.PlayerX;
    private int moves = 0;
    private boolean firstTime=false;
    private Util myUtil;

    private void changeTurn(){
        this.turn = this.turn == Player.PlayerX ? Player.PlayerO : Player.PlayerX;
    }

    public TicTacToeApp(TicTacToe ttt) throws Exception {
        this.ttt=ttt;
        myView = new TTTView(this);
    }


    public void computerMove(){

        int prologIndex=-1;
        if(GameSettings.getInstance().checkMode().equals("avg")){
            prologIndex=ttt.checkAvgComputer();
        }else if(GameSettings.getInstance().checkMode().equals("hard")){
            prologIndex=ttt.checkHardComputer();
        }

        if(prologIndex!=-1 && !firstTime){
            //System.out.println("CIAO: "+ttt.checkHardComputer());

                    myUtil=new Util();
            myUtil.convertIndexFromPrologToJava(prologIndex);

            if (ttt.move(turn, myUtil.y(),myUtil.x())){
                myView.getBoard()[myUtil.y()][myUtil.x()].setText("O");
                moves++;
                changeTurn();
                firstTime=!firstTime;
                if (moves > 3){
                    System.out.println("X winning count: "+ttt.winCount(turn, Player.PlayerX));
                    System.out.println("O winning count: "+ttt.winCount(turn, Player.PlayerO));
                }
            }
        }else{
            computerDummyMove();
        }

        checkVictoryOrDraw();
    }


    private void computerDummyMove(){
        int i,j;

        do{
            i = 0 + (int)(Math.random() * 3);
            j = 0 + (int)(Math.random() * 3);

        } while(myView.getBoard()[i][j].getText() != "");


        if (ttt.move(turn,i,j)){
            myView.getBoard()[i][j].setText("O");
            moves++;

            changeTurn();
            if (moves > 3){
                System.out.println("X winning count: "+ttt.winCount(turn, Player.PlayerX));
                System.out.println("O winning count: "+ttt.winCount(turn, Player.PlayerO));
            }
        }
        checkVictoryOrDraw();
    }

    public void humanMove(int i, int j){
        if (ttt.move(turn,i,j)){
            if(GameSettings.getInstance().checkMode().equals("computer") || GameSettings.getInstance().checkMode().equals("avg")) {
                myView.getBoard()[i][j].setText("X");
            }else{
                myView.getBoard()[i][j].setText(turn == Player.PlayerX ? "X" : "O");
            }

            moves++;
            changeTurn();
            if (moves > 3){
                System.out.println("X winning count: "+ttt.winCount(turn, Player.PlayerX));
                System.out.println("O winning count: "+ttt.winCount(turn, Player.PlayerO));
            }
        }
        checkVictoryOrDraw();
        if(GameSettings.getInstance().checkMode().equals("computer")) {
            this.computerDummyMove();
        }else if(GameSettings.getInstance().checkMode().equals("avg") || GameSettings.getInstance().checkMode().equals("hard")) {
            this.computerMove();
        }
    }

    private void checkVictoryOrDraw(){
        Optional<Player> victory = ttt.checkVictory();
        if (victory.isPresent()){
            myView.getExit().setText(victory.get()+" won!");
            finished=true;
            return;
        }
        if (ttt.checkCompleted()){
            myView.getExit().setText("Even!");
            finished=true;
            return;
        }

    }

    public boolean isFinished() {
        return finished;
    }


}
