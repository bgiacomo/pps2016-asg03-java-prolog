% symbol(+OnBoard,-OnScreen): symbols for the board and their rendering
symbol(null,'_').
symbol(p1,'X').
symbol(p2,'O').

% result(+OnBoard,-OnScreen): result of a game
result(even,"even!").
result(p1,"player 1 wins").
result(p2,"player 2 wins").

% other_player(?Player,?OtherPlayer)
other_player(p1,p2).
other_player(p2,p1).

% render(+List): prints a TTT table (9 elements list) on console
render(L) :- convert_symbols(L,[A,B,C,D,E,F,G,H,I]),print_row(A,B,C),print_row(D,E,F),print_row(G,H,I).
convert_symbols(L,L2) :- findall(R,(member(X,L),symbol(X,R)),L2).
print_row(A,B,C) :- put(A),put(' '),put(B),put(' '),put(C),nl.

% render(+List,+Result): prints a TTT table plus result
render_full(L,Result) :- result(Result,OnScreen),print(OnScreen),nl,render(L),nl,nl.

% create_board(-Board): creates an initially empty board
create_board(B):-create_list(9,null,B).
create_list(0,_,[]) :- !.
create_list(N,X,[X|T]) :- N2 is N-1, create_list(N2,X,T).

% next_board(+Board,+Player,?NewBoard): finds (zero, one or many) new boards as Player moves
next_board([null|B],PL,[PL|B]).
next_board([X|B],PL,[X|B2]):-next_board(B,PL,B2).

% final(+Board,-Result): checks where the board is final and why
final(B,p1) :- finalpatt(P), match(B,P,p1),!.
final(B,p2) :- finalpatt(P), match(B,P,p2),!.
final(B,even) :- not(member(null,B)).

% finalVert(+Board,-Result): checks where the board is final and why
finalVert(B,p1) :- finalVertPatt(P), match(B,P,p1),!.
finalVert(B,p2) :- finalVertPatt(P), match(B,P,p2),!.
finalVert(B,even) :- not(member(null,B)).

% finalHor(+Board,-Result): checks where the board is final and why
finalHor(B,p1) :- finalHorPatt(P), match(B,P,p1),!.
finalHor(B,p2) :- finalHorPatt(P), match(B,P,p2),!.
finalHor(B,even) :- not(member(null,B)).

% match(Board,Pattern,Player): checks if in the board, the player matches a winning pattern
match([],[],_).
match([M|B],[x|P],M):-match(B,P,M).
match([_|B],[o|P],M):-match(B,P,M).

% CLASSIC MODE finalpatt(+Pattern): gives a winning pattern
finalpatt([x,x,x,o,o,o,o,o,o]).
finalpatt([o,o,o,x,x,x,o,o,o]).
finalpatt([o,o,o,o,o,o,x,x,x]).
finalpatt([x,o,o,x,o,o,x,o,o]).
finalpatt([o,x,o,o,x,o,o,x,o]).
finalpatt([o,o,x,o,o,x,o,o,x]).
finalpatt([x,o,o,o,x,o,o,o,x]).
finalpatt([o,o,x,o,x,o,x,o,o]).

% VERTICAL MODE
finalVertPatt([x,x,x,o,o,o,o,o,o]).
finalVertPatt([o,o,o,x,x,x,o,o,o]).
finalVertPatt([o,o,o,o,o,o,x,x,x]).

% HORIZONTAL MODE
finalHorPatt([x,o,o,x,o,o,x,o,o]).
finalHorPatt([o,x,o,o,x,o,o,x,o]).
finalHorPatt([o,o,x,o,o,x,o,o,x]).

% game(+Board,+Player,-FinalBoard,-Result): finds one (zero, one or many) final boards and results
game(B,_,B,Result) :- final(B,Result),!.
game(B,PL,BF,Result):- next_board(B,PL,B2), other_player(PL,PL2),game(B2,PL2,BF,Result).

% statistics(+Board,+Player,+Result,-Count): counts how many time Res will happen 
statistics(B,P,Res,Count) :- findall(a, game(B,P,_,Res),L), length(L,Count).


%PATTERN AVG MODE

% finalAvg(+Board,-Result,-index): checks if the move matches with movepatt - added index of the cell to tick
finalAvg(B,p1,S) :- finalpattAvg(P,S), matchMoveAvg(B,P,p1),!.

% matchMove(Board,Pattern,Player, index): checks if in the board, the player matches a winning pattern - added index
matchMoveAvg([],[],_).
matchMoveAvg([M|B],[x|P],M):-matchMoveAvg(B,P,M).
matchMoveAvg([_|B],[o|P],M):-matchMoveAvg(B,P,M).

finalpattAvg([x,x,o,o,o,o,o,o,o],S):-S is 6.
finalpattAvg([o,x,x,o,o,o,o,o,o],S):-S is 0.
finalpattAvg([x,o,x,o,o,o,o,o,o],S):-S is 3.
finalpattAvg([o,o,o,x,x,o,o,o,o],S):-S is 7.
finalpattAvg([o,o,o,o,x,x,o,o,o],S):-S is 1.
finalpattAvg([o,o,o,x,o,x,o,o,o],S):-S is 4.
finalpattAvg([o,o,o,o,o,o,x,x,o],S):-S is 8.
finalpattAvg([o,o,o,o,o,o,x,o,x],S):-S is 5.
finalpattAvg([o,o,o,o,o,o,o,x,x],S):-S is 2.

%PATTERN HARD MODE

% finalHard(+Board,-Result,-index): checks if the move matches with movepatt - added index of the cell to tick
finalHard(B,p1,S) :- finalpattHard(P,S), matchMove(B,P,p1),!.

% matchMove(Board,Pattern,Player, index): checks if in the board, the player matches a winning pattern - added index
matchMove([],[],_).
matchMove([M|B],[x|P],M):-matchMove(B,P,M).
matchMove([_|B],[o|P],M):-matchMove(B,P,M).

% finalpattHard(+Pattern): gives a move pattern
finalpattHard([o,x,x,o,o,o,o,o,o],S):-S is 0.
finalpattHard([o,o,o,x,o,o,x,o,o],S):-S is 0.
finalpattHard([o,o,o,o,x,x,o,o,o],S):-S is 1.
finalpattHard([x,o,o,o,o,o,x,o,o],S):-S is 1.
finalpattHard([x,o,o,x,o,o,o,o,o],S):-S is 2.
finalpattHard([o,o,o,o,o,o,o,x,x],S):-S is 2.
finalpattHard([x,o,x,o,o,o,o,o,o],S):-S is 3.
finalpattHard([o,o,o,o,x,o,o,x,o],S):-S is 3.
finalpattHard([o,o,o,x,o,x,o,o,o],S):-S is 4.
finalpattHard([o,x,o,o,o,o,o,x,o],S):-S is 4.
finalpattHard([o,o,o,o,o,o,x,o,x],S):-S is 5.
finalpattHard([o,x,o,o,x,o,o,o,o],S):-S is 5.
finalpattHard([x,x,o,o,o,o,o,o,o],S):-S is 6.
finalpattHard([o,o,o,o,o,x,o,o,x],S):-S is 6.
finalpattHard([o,o,o,x,x,o,o,o,o],S):-S is 7.
finalpattHard([o,o,x,o,o,o,o,o,x],S):-S is 7.
finalpattHard([o,o,o,o,o,o,x,x,o],S):-S is 8.
finalpattHard([o,o,x,o,o,x,o,o,o],S):-S is 8.
finalpattHard([x,o,o,o,x,o,o,o,o],S):-S is 8.
finalpattHard([o,o,o,o,x,o,o,o,x],S):-S is 0.
finalpattHard([x,o,o,o,o,o,o,o,x],S):-S is 4.

