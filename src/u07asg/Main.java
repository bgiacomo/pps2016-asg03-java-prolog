package u07asg;

/**
 * Created by Giacomo on 25/04/2017.
 */
public class Main {

    public static void main(String[] args){
        GameSettings.getInstance();
        try {
            new TicTacToeApp(new TicTacToeImpl("src/u07asg/ttt.pl"));
        } catch (Exception e) {
            System.out.println("Problems loading the theory");
        }
    }
}
