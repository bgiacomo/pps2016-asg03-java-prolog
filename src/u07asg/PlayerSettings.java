package u07asg;

/**
 * Created by Giacomo on 26/04/2017.
 */
public class PlayerSettings {

    private boolean isClassicMode;
    private boolean isVerticalMode;
    private boolean isHorizontalMode;

    public PlayerSettings() {
        this.isClassicMode = true;
        this.isVerticalMode = false;
        this.isHorizontalMode = false;
    }

    public boolean isClassicMode() {
        return isClassicMode;
    }

    public boolean isVerticalMode() {
        return isVerticalMode;
    }

    public boolean isHorizontalMode() {
        return isHorizontalMode;
    }

    public void setClassicMode(boolean classicMode) {
        isClassicMode = classicMode;
        isVerticalMode=false;
        isHorizontalMode=false;
    }

    public void setVerticalMode(boolean verticalMode) {
        isVerticalMode = verticalMode;
        isClassicMode=false;
        isHorizontalMode=false;
    }

    public void setHorizontalMode(boolean horizontalMode) {
        isHorizontalMode = horizontalMode;
        isVerticalMode=false;
        isClassicMode=false;
    }
}
