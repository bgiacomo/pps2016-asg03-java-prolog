package u07asg;

import javax.swing.*;
import java.awt.*;


/**
 * Created by Giacomo on 25/04/2017.
 */
public class TTTView {

    private final static int BOARD_HEIGHT = 3;
    private final static int BOARD_WIDTH = 3;

    private final JButton[][] board = new JButton[BOARD_WIDTH][BOARD_HEIGHT];
    private final JButton exit = new JButton("Exit");
    private final JFrame frame = new JFrame("Tic Tac Toe");
    private JMenuBar menuBar = new JMenuBar();
    private JRadioButtonMenuItem classicModeMenuItem;
    private JRadioButtonMenuItem verticalModeMenuItem;
    private JRadioButtonMenuItem horizontalModeMenuItem;
    private JRadioButtonMenuItem computerDummyModeMenuItem;
    private JRadioButtonMenuItem computerAvgModeMenuItem;
    private JRadioButtonMenuItem computerHardModeMenuItem;
    private JMenu options;
    private JMenu submenu;
    private JMenu secondSubmenu;

    private TicTacToeApp ttt;


    public TTTView(TicTacToeApp ttt) {
        initPane();
        this.ttt=ttt;
    }

    private void addRadioButtonToMenu(){

        submenu = new JMenu("1 vs 1");
        options.add(submenu);
        secondSubmenu = new JMenu("1 vs CPU");
        options.add(secondSubmenu);

        ButtonGroup group = new ButtonGroup();
        ButtonGroup computerGroup=new ButtonGroup();

        classicModeMenuItem = new JRadioButtonMenuItem("Classic Mode");
        classicModeMenuItem.setSelected(true);
        group.add(classicModeMenuItem);

        verticalModeMenuItem = new JRadioButtonMenuItem("Vertical Mode");
        group.add(verticalModeMenuItem);

        horizontalModeMenuItem = new JRadioButtonMenuItem("Horizontal Mode");
        group.add(horizontalModeMenuItem);

        computerDummyModeMenuItem = new JRadioButtonMenuItem("Dummy");
        computerGroup.add(computerDummyModeMenuItem);
        secondSubmenu.add(computerDummyModeMenuItem);

        computerAvgModeMenuItem = new JRadioButtonMenuItem("Average");
        computerGroup.add(computerAvgModeMenuItem);
        secondSubmenu.add(computerAvgModeMenuItem);

        computerHardModeMenuItem = new JRadioButtonMenuItem("Hard");
        computerGroup.add(computerHardModeMenuItem);
        secondSubmenu.add(computerHardModeMenuItem);


        setListenersToRadioButton();

        submenu.add(classicModeMenuItem);
        submenu.add(verticalModeMenuItem);
        submenu.add(horizontalModeMenuItem);

    }

    private void setListenersToRadioButton(){
        classicModeMenuItem.addActionListener(e -> GameSettings.getInstance().switchToClassicMode());
        verticalModeMenuItem.addActionListener(e -> GameSettings.getInstance().switchToVerticalMode());
        horizontalModeMenuItem.addActionListener(e -> GameSettings.getInstance().switchToHorizontalMode());
        computerDummyModeMenuItem.addActionListener(e -> GameSettings.getInstance().switchToComputerDummyMode());
        computerAvgModeMenuItem.addActionListener(e -> GameSettings.getInstance().switchToComputerAvgMode());
        computerHardModeMenuItem.addActionListener(e -> GameSettings.getInstance().switchToComputerHardMode());
    }

    private void setupMenu(){
        options = new JMenu("Options"); // Create File menu
        menuBar.add(options); // Add the file menu
    }

    private void initPane(){
        frame.setLayout(new BorderLayout());
        JPanel b=new JPanel(new GridLayout(3,3));
        for (int i=0;i<3;i++){
            for (int j=0;j<3;j++){
                final int i2 = i;
                final int j2 = j;
                board[i][j]=new JButton("");
                b.add(board[i][j]);
                board[i][j].addActionListener(e -> { if (!ttt.isFinished()){
                    ttt.humanMove(i2,j2);
                }});
            }
        }
        JPanel s=new JPanel(new FlowLayout());
        s.add(exit);
        setupMenu();
        frame.setJMenuBar(menuBar);
        addRadioButtonToMenu();

        exit.addActionListener(e -> System.exit(0));
        frame.add(BorderLayout.CENTER,b);
        frame.add(BorderLayout.SOUTH,s);
        frame.setSize(200,230);
        frame.setVisible(true);
    }



    public JButton[][] getBoard() {
        return board;
    }

    public JButton getExit() {
        return exit;
    }
}
